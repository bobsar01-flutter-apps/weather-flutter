import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter/material.dart';

class Alerts {
  BuildContext context;

  AlertStyle _alertStyle(Color textColor) {
    return AlertStyle(
      animationType: AnimationType.fromTop,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      animationDuration: Duration(milliseconds: 10),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: textColor,
      ),
    );
  }

// Alert with single button.
  _alertButtonBasic(context, String title, String description, Color textColor,
      AlertType alertType, String btnText, Function onClick) {
    Alert(
      context: context,
      type: alertType,
      title: title,
      style: _alertStyle(textColor),
      desc: description,
      buttons: [
        DialogButton(
          color: Colors.blue,
          child: Text(
            btnText,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: onClick,
          width: 120,
        )
      ],
    ).show();
  }

  void getAnswerAlertBasic(
      {buildContext,
      String title,
      String description,
      Color textColor,
      AlertType alertType,
      String btnText,
      Function onClick}) {
    _alertButtonBasic(buildContext, title, description, textColor, alertType,
        btnText, onClick);
  }
}
